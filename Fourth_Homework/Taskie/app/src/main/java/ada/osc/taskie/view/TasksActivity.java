package ada.osc.taskie.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.List;

import ada.osc.taskie.R;
import ada.osc.taskie.TaskRepository;
import ada.osc.taskie.model.Task;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TasksActivity extends AppCompatActivity {

    private static final String TAG = TasksActivity.class.getSimpleName();
    private static final int REQUEST_NEW_TASK = 10;
    public static final String EXTRA_TASK = "task";
    public static final String TASK_EXTRA = "updated";



    TaskRepository mRepository = TaskRepository.getInstance();
    TaskAdapter mTaskAdapter;

    @BindView(R.id.fab_tasks_addNew)
    FloatingActionButton mNewTask;
    @BindView(R.id.recycler_tasks)
    RecyclerView mTasksRecycler;
    @BindView(R.id.spinner_sortbypriority)
    Spinner mSortByPriority;
    @BindView(R.id.spinner_show)
    Spinner mShowTask;


    TaskClickListener mListener = new TaskClickListener() {
        @Override
        public void onClick(Task task) {
            toastTask(task);
            updateTask(task);
//            Bundle extras = new Bundle();
//            extras.putString("EXTRA_TITLE", task.getTitle().toString());
//            extras.putString("EXTRA_DESCRIPTION", task.getDescription().toString());
//            extras.putString("EXTRA_PRIORITY", task.getPriority().toString());
//            extras.putString("EXTRA_DATE", task.getMdate().toString());
//            Intent updateTask = new Intent();
//            updateTask.putExtras(extras);
//            updateTask.setClass(getApplicationContext(), NewTaskActivity.class);
//            startActivityForResult(updateTask, REQUEST_NEW_TASK);
        }

        @Override
        public void onLongClick(Task task) {
            mRepository.removeTask(task);
            updateTasksDisplay();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);

        ButterKnife.bind(this);
        setUpRecyclerView();
        updateTasksDisplay();
        sortByPriority();
        showTasks();
    }


    private void setUpRecyclerView() {

        int orientation = LinearLayoutManager.VERTICAL;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                this,
                orientation,
                false
        );

        RecyclerView.ItemDecoration decoration =
                new DividerItemDecoration(this, orientation);

        RecyclerView.ItemAnimator animator = new DefaultItemAnimator();

        mTaskAdapter = new TaskAdapter(mListener);

        mTasksRecycler.setLayoutManager(layoutManager);
        mTasksRecycler.addItemDecoration(decoration);
        mTasksRecycler.setItemAnimator(animator);
        mTasksRecycler.setAdapter(mTaskAdapter);
    }

    private void updateTasksDisplay() {
        List<Task> tasks = mRepository.getTasks();
        mTaskAdapter.updateTasks(tasks);
        for (Task t : tasks) {
            Log.d(TAG, t.getTitle());
        }
    }

    private void toastTask(Task task) {
        Toast.makeText(
                this,
                task.getTitle() + "\n" + task.getDescription(),
                Toast.LENGTH_SHORT
        ).show();
    }


    @OnClick(R.id.fab_tasks_addNew)
    public void startNewTaskActivity() {
        Intent newTask = new Intent();
        newTask.setClass(this, NewTaskActivity.class);
        startActivityForResult(newTask, REQUEST_NEW_TASK);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_NEW_TASK && resultCode == RESULT_OK) {
            if (data != null && data.hasExtra(EXTRA_TASK)) {
                Task task = (Task) data.getSerializableExtra(EXTRA_TASK);
                mRepository.saveTask(task);
                updateTasksDisplay();
            }
            else if (data != null && data.hasExtra(TASK_EXTRA)) {
                Task task = (Task) data.getSerializableExtra(TASK_EXTRA);
                mRepository.findTask(task.getId());
                mRepository.saveTask(task);
                updateTasksDisplay();
            }
        }

    }

    public void updateTask(Task task) {
        Intent intent = new Intent();
        intent.setClass(this, NewTaskActivity.class);
        intent.putExtra(TASK_EXTRA, task);
        startActivityForResult(intent, REQUEST_NEW_TASK);
    }

    private void sortByPriority() {
        ArrayAdapter<CharSequence> priorityAdapter = ArrayAdapter.createFromResource(this, R.array.sortbypriority,
                android.R.layout.simple_spinner_item);
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        mSortByPriority.setAdapter(priorityAdapter);

        String spinnerValue = mSortByPriority.getSelectedItem().toString();


    }

    private void showTasks() {
        ArrayAdapter<CharSequence> showAdapter = ArrayAdapter.createFromResource(this, R.array.showtasks,
                android.R.layout.simple_spinner_item);
        showAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mShowTask.setAdapter(showAdapter);

    }

}
