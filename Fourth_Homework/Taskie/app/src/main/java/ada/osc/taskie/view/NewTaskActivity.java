package ada.osc.taskie.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import ada.osc.taskie.R;
import ada.osc.taskie.model.Task;
import ada.osc.taskie.model.TaskPriority;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewTaskActivity extends AppCompatActivity {

    @BindView(R.id.edittext_newtask_title)
    EditText mTitleEntry;
    @BindView(R.id.edittext_newtask_description)
    EditText mDescriptionEntry;
    @BindView(R.id.spinner_newtask_priority)
    Spinner mPriorityEntry;
    @BindView(R.id.imagebutton_newtask_savetask)
    ImageButton btn;
    @BindView(R.id.textview_chosendate)
    TextView mChosenDate;


    private Task updated;
    private boolean isUpdated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);
        ButterKnife.bind(this);
        setUpSpinnerSource();

        Intent intent = getIntent();
        if (intent.getSerializableExtra(TasksActivity.TASK_EXTRA) != null) {
            updated = (Task) intent.getSerializableExtra(TasksActivity.TASK_EXTRA);
            mTitleEntry.setText(updated.getTitle());
            mDescriptionEntry.setText(updated.getDescription());
            mChosenDate.setText(updated.getMdate().toString());

            isUpdated = true;
        }
    }

    private void setUpSpinnerSource() {
        mPriorityEntry.setAdapter(
                new ArrayAdapter<TaskPriority>(
                        this, android.R.layout.simple_list_item_1, TaskPriority.values()
                )
        );
        mPriorityEntry.setSelection(0);
    }

    @OnClick(R.id.imagebutton_newtask_savetask)
    public void saveTask() {
        String title = mTitleEntry.getText().toString();
        String description = mDescriptionEntry.getText().toString();
        TaskPriority priority = (TaskPriority) mPriorityEntry.getSelectedItem();
        String date = mChosenDate.getText().toString();

        String check = checkInput(title, description, date);
        Intent saveTaskIntent = new Intent(this, TasksActivity.class);
        if (check.equals("Ok")) {
            if (!isUpdated) {
                Task newTask = new Task(title, description, priority, date);
                saveTaskIntent.putExtra(TasksActivity.EXTRA_TASK, newTask);
            } else {
                updated.setTitle(mTitleEntry.getText().toString());
                updated.setDescription(mDescriptionEntry.getText().toString());
                updated.setPriority((TaskPriority) mPriorityEntry.getSelectedItem());
                updated.setMdate(mChosenDate.getText().toString());
                saveTaskIntent.putExtra(TasksActivity.TASK_EXTRA, updated);

            }
            setResult(RESULT_OK, saveTaskIntent);
            finish();
        } else {
            Toast.makeText(this, check, Toast.LENGTH_SHORT).show();
        }
    }

    private String checkInput(String title, String description, String date) {

        if (title.equals("")) {
            return "Title empty";
        } else if (description.equals("")) {
            return "Description empty";
        } else if (date.equals("")) {
            return "Date empty";
        } else return "Ok";

    }

    @OnClick(R.id.imagebutton_choosedate)
    public void chooseDate() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final DatePicker picker = new DatePicker(this);
        picker.setCalendarViewShown(false);
        picker.setMinDate(System.currentTimeMillis() - 1000);

        builder.setTitle(R.string.choose_final_date)
                .setView(picker)
                .setPositiveButton(R.string.set_date, new DialogInterface.OnClickListener() {
                    @OnClick
                    public void onClick(DialogInterface dialog, int which) {
                        int month = picker.getMonth() + 1;
                        int day = picker.getDayOfMonth();
                        int year = picker.getYear();
                        mChosenDate.setText(new StringBuilder().append(day)
                                .append(".").append(month).append(".").append(year)
                                .append(" "));
                    }
                })
                .show();
    }
}
