package ada.osc.taskie.model;

import java.io.Serializable;
import java.util.UUID;

public class Task implements Serializable {

    private static int sID = 0;

    String mId;
    private String mTitle;
    private String mDescription;
    private boolean mCompleted;
    private TaskPriority mPriority;
    private String mdate;

    public Task() {

    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public Task(String title, String description, TaskPriority priority, String date) {
        mId = UUID.randomUUID().toString();
        mTitle = title;
        mDescription = description;
        mCompleted = false;
        mPriority = priority;
        mdate = date;
    }

    public Task(String mTitle, String mDescription, TaskPriority mPriority) {
        this.mId = mId;
        this.mTitle = mTitle;
        this.mDescription = mDescription;
        this.mPriority = mPriority;
    }

    public String getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public boolean isCompleted() {
        return mCompleted;
    }

    public void setCompleted(boolean completed) {
        mCompleted = completed;
    }

    public TaskPriority getPriority() {
        return mPriority;
    }

    public void setPriority(TaskPriority priority) {
        mPriority = priority;
    }


}
