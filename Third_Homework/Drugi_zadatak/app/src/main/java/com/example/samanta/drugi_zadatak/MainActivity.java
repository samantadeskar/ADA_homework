package com.example.samanta.drugi_zadatak;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @BindView(R.id.etEnterFirstNumber)
    EditText etEnterFirstNumber;
    @BindView(R.id.etEnterSecondNubmber)
    EditText etEnterSecondNumber;
    @BindView(R.id.bSum)
    Button bSum;
    @BindView(R.id.bSubtraction)
    Button bSubstraction;
    @BindView(R.id.bMultiplication)
    Button bMultiplication;
    @BindView(R.id.bDivision)
    Button bDivision;
    @BindView(R.id.tvResult)
    TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.bSum, R.id.bSubtraction, R.id.bMultiplication, R.id.bDivision})
    public void calculate(Button btn) {
        double firstNumber = Double.parseDouble(etEnterFirstNumber.getText().toString());
        double secondNumber = Double.parseDouble(etEnterSecondNumber.getText().toString());


        switch (btn.getId()) {

            case R.id.bSum:
                double sum = Double.parseDouble(etEnterFirstNumber.getText().toString()) + Double.parseDouble(etEnterSecondNumber.getText().toString());
                this.tvResult.setText(String.valueOf(sum));
                break;
            case R.id.bSubtraction:
                double sub = Double.parseDouble(etEnterFirstNumber.getText().toString()) - Double.parseDouble(etEnterSecondNumber.getText().toString());
                this.tvResult.setText(String.valueOf(sub));
                break;

            case R.id.bMultiplication:
                double mult = Double.parseDouble(etEnterFirstNumber.getText().toString()) * Double.parseDouble(etEnterSecondNumber.getText().toString());
                this.tvResult.setText(String.valueOf(mult));
                break;

            case R.id.bDivision:
                double div = Double.parseDouble(etEnterFirstNumber.getText().toString()) / Double.parseDouble(etEnterSecondNumber.getText().toString());
                this.tvResult.setText(String.valueOf(div));
                break;

        }
    }
}
