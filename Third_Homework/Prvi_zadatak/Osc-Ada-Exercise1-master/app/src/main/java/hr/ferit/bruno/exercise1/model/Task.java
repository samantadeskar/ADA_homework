package hr.ferit.bruno.exercise1.model;

public class Task {
	private int mImportance;
	private String mTitle;
	private String mSummary;

	public Task(String title, String summary, int importance) {
		mTitle = title;
		mSummary = summary;
		mImportance = importance;
	}

	public int getImportance() {
		return mImportance;
	}

	public void setImportance(int importance) {
		mImportance = importance;
	}

	public String getTitle() {
		return mTitle;
	}

	public void setTitle(String title) {
		mTitle = title;
	}

	public String getSummary() {
		return mSummary;
	}

	public void setSummary(String summary) {
		mSummary = summary;
	}

	@Override
	public String toString() {
		return "Task: " + mTitle +"\t"+"\t"+
                "Summary: " + mSummary +"\t"+"\t"+
                "Importance:" + mImportance+
		        "\n";
	}
}
