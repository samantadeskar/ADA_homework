package com.example.samanta.cetvrti_zadatak;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @BindView(R.id.etHeight)
    EditText etHeight;
    @BindView(R.id.etWeight)
    EditText etWeight;
    @BindView(R.id.tvResult)
    TextView tvResult;
    @BindView(R.id.tvResultTitle)
    TextView tvResultTitle;
    @BindView(R.id.tvResultDescription)
    TextView tvResultDescription;
    @BindView(R.id.btnCalculate)
    Button btnCalculate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnCalculate)
    public void calculateBMI() {

        double height = Double.parseDouble(etHeight.getText().toString());
        double weight = Double.parseDouble(etWeight.getText().toString());

        double result = weight / (height * height);

        if (result < 18.5) {
            tvResult.setText(String.valueOf(result));
            tvResultTitle.setText(R.string.underweight_title);
            tvResultDescription.setText(R.string.underweight_description);
        } else if (result >= 18.5 && result < 25) {
            tvResult.setText(String.valueOf(result));
            tvResultTitle.setText(R.string.normal);
            tvResultDescription.setText(R.string.normal_description);
        } else if (result >= 25 && result < 30) {
            tvResult.setText(String.valueOf(result));
            tvResultTitle.setText(R.string.overweight);
            tvResultDescription.setText(R.string.overweight_description);
        } else {
            tvResult.setText(String.valueOf(result));
            tvResultTitle.setText(R.string.obese);
            tvResultDescription.setText(R.string.obese_description);
        }

    }
}
