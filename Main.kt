/**
 * Created by Filip on 07/04/2018.
 */


//nemamo klasu u kojoj se mora najaviti main, sve se radi u fileovima

fun main(args: Array<String>) {

    //tip podataka (mutability), type inference
    var number = 5

    number = 6 //okay

    val name = "Filip"
    name = "Pilip" //nope -> val je nepromjenjiv


    val lastName: String = ""


    //assignement preko expressiona
    val startsWithAnF = if (name.startsWith("F")) {
        //do some assignement

        true
    } else {
        false //nope
    }

    val startsWithF = if (name.startsWith("F")) "Yep" else "Nope"

    val startsWithP = lastName.startsWith("P")

    //kreiranje arrayeva
    val array = arrayOf(5, 4, 2, 1, 9, 5) //nema array literala [1,2,3,4]

    for (i in array) { //iteriranje kroz "rangeove", klasican foreach loop
        println(i)
    }

    // in ukljucuje i zadnji index/element -> i in 1..10 znaci 1 do 10, ukljucujuci i 10

    for (i in 0..array.lastIndex) { //klasican for loop s malo drukcijom sintaksom
        println(array[i])
    }

    for (i in 0..array.lastIndex step 2) { //for loop, gdje svaka iteracija inkrementira i za 2

    }

    for (i in 0 until array.size) { //until iskljucuje zadnju vrijednost rangea

    }

    array.forEach { i -> print(i) } //koristenjem lambda funkcija i funkcijskih operatora
    array.forEach { print(it) } // kraci zapis, ako imamo samo jednu varijablu u lambdi
    array.forEach(::print) //najkraci zapis, samo proslijedi vrijednost u funkciju print

    //flow control

    val condition = true

    if (condition) {
        //do something
    } else {
        //do something else
    }

    when (condition) { //provjera varijable
        true -> {
            //doSomething
        }

        false -> {
            //doSomethingElse
        }
    }

    when {
    //nizamo uvjete
        condition -> {
            //doSomething
        }

        System.currentTimeMillis() > 1000000 -> {
            //something else
        }
    }
}