package com.example.samanta.subota2104;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName() ;
    @BindView(R.id.etPassword) EditText etPassword;
    @BindView(R.id.btnSave) Button btnSave;
    @BindView(R.id.tvDisplay) TextView tvDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnSave)
    public void savePassword(){
        String text = etPassword.getText().toString();
        tvDisplay.setText(text);
        displayMessage(text);
        logMessage(text);
    }

    private void logMessage(String text) {
        Log.d(TAG, text);
        Log.e(TAG, text);
        Log.w(TAG, text);
    }

    private void displayMessage(String text){
        Toast.makeText(this,text,Toast.LENGTH_SHORT).show();
    }

}