package deskar;

public class Task3 {

//Write a program that prints a multiplication table for numbers up to 12.

    public static void main(String[] args) {

        for (int i = 1; i <= 12; i++) {
            for (int j = 1; j <= 12; j++) {
                System.out.print((i*j) + "\t");
            }
            System.out.println();
        }

    }
}
