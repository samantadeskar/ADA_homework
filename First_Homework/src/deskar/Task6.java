package deskar;

import javax.swing.*;

public class Task6 {

    //Write a method that reverses a array.

    public static void main(String[] args) {

        int n = Integer.parseInt(JOptionPane.showInputDialog("Enter wanted length of array: "));
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(JOptionPane.showInputDialog("Enter number"));
        }

        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}

