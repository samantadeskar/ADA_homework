package deskar;

import javax.swing.*;

public class Task2 {

    //Write a program that asks the user for a number n and gives them the possibility
    // to choose between computing the sum and computing the product of 1,…,n.

    public static void main(String[] args) {
        int n, choose;

        for (; ; ) {
            try {
                n = Integer.parseInt(JOptionPane.showInputDialog("Enter number n:"));
                do {
                    choose = Integer.parseInt(JOptionPane.showInputDialog("What do you want?" +
                            "\n1 - computing the sum between numbers 1 and " + n +
                            "\n2 - computing the product between numbers 1 and " + n));
                } while (choose != 1 & choose != 2);
                break;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Enter valid number");
            }
        }
        switch (choose) {
            case 1:
                int sum = 0;
                for (int i = 1; i <= n; i++) {
                    sum = sum + i;
                }
                JOptionPane.showMessageDialog(null, "Sum of numbers between 1 and n is: " + sum);
                break;
            case 2:
                int product = 1;
                for (int i = 1; i <= n; i++) {
                    product = product * i;
                }
                JOptionPane.showMessageDialog(null, "Product of numbers between 1 and n is: " + product);
                break;


        }
    }
}
