package deskar;

public class Task4 {

    //Write a program that prints the next 20 leap years.


    public static void main(String[] args) {
        int sum = 0;
        for (int i = 2018; i <= 2200; i++) {
            if (i % 4 == 0 || i % 100 == 0 || i % 400 == 0) {
                if (sum < 20) {
                    System.out.println(i);
                    sum++;
                }
            }
        }

    }
}
