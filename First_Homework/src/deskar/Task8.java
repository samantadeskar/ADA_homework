package deskar;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;

public class Task8 {
    //Write a method that takes a int number and returns a array of its digits.

    public static void main(String[] args) {

        int num;
        for (; ; ) {
            try {
                num = Integer.parseInt(JOptionPane.showInputDialog("Enter integer number"));
                break;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Enter valid number");
            }
        }

        ArrayList<Integer> array = new ArrayList<>();
        do {
            array.add(num % 10);
            num /= 10;
        } while (num > 0);

        Collections.reverse(array);
        JOptionPane.showMessageDialog(null, "Array of digits is: " + array);
    }
}
