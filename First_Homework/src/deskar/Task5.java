package deskar;

import javax.swing.*;

public class Task5 {

    //Write a method that returns the largest element in a array.

    public static void main(String[] args) {

        int n = Integer.parseInt(JOptionPane.showInputDialog("Enter wanted length of array: "));
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(JOptionPane.showInputDialog("Enter number"));
        }
        int max = 0;
        for (int i = 0; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
            }
        }
        JOptionPane.showMessageDialog(null, "Largest element in the array is " + max);

    }
}
