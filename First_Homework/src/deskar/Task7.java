package deskar;

import javax.swing.*;

public class Task7 {

    //Write a method that returns the elements on odd positions in a array


    public static void main(String[] args) {
        int n = Integer.parseInt(JOptionPane.showInputDialog("Enter wanted length of array: "));
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(JOptionPane.showInputDialog("Enter number"));
        }

        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 1) {
                System.out.println("On position " + i + " is number " + array[i]);
            }
        }
    }

}
