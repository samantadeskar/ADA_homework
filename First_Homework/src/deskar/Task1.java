package deskar;

import javax.swing.*;

public class Task1 {

    //Write a program that asks the user for a number n and prints the sum of the numbers 1 to n


    public static void main(String[] args) {
        int sum = 0;

        int n = Integer.parseInt(JOptionPane.showInputDialog("Enter number n: "));

        for (int i = 1; i <= n; i++) {
            sum += i;
        }
        JOptionPane.showMessageDialog(null, "Sum of numbers between 1 and n is: " + sum);

    }

}
