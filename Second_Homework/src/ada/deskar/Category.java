package ada.deskar;

public class Category {

    private String title;


    public Category(String title) {
        this.title = title;
    }

    public Category() {

    }

    public String getCategory() {
        return title;
    }

    public void setCategory(String category) {
        this.title = category;
    }

    @Override
    public String toString() {
        return  title;
    }
}
