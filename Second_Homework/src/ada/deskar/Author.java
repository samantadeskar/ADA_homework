package ada.deskar;

public class Author {

    private String name;


    public Author(String name, String surname) {

        this.name = name;
    }

    public Author() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
