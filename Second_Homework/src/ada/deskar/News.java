package ada.deskar;

import java.util.List;

public class News {
    private String title;
    private String text;
    private List<Category> categories;
    private Author author;
    private String date;

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Author getAuthor() {
        return author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Author getAuthors() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategorie(List<Category> categories) {
        this.categories = categories;
    }

    public void addCategory(Category category){
        this.categories.add(category);
    }

    @Override
    public String toString() {
        return "News:\n" +
                "title: '" + title + '\'' +
                ", text: '" + text + '\'' +
                ", categories: " + categories +
                ", author: " + author;
    }
}
