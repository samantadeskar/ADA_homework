package osc.ada.deskar;

public class Pjesma {

    private String izvodac;
    private String naziv;

    public String getIzvodac() {
        return izvodac;
    }

    public void setIzvodac(String izvodac) {
        this.izvodac = izvodac;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
}
