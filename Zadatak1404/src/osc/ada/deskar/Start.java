package osc.ada.deskar;

import java.util.*;

public class Start {

    public static void main(String[] args) {

        List<Evidencija> evidencije = new ArrayList<>();

        Scanner scan = new Scanner(System.in);
        Evidencija e;
        Pjesma p;
        List<Pjesma> count = new ArrayList<>();
        while (true) {
            System.out.println("Unesi naziv evidencije: ");
            e = new Evidencija();
            e.setNaziv(scan.nextLine());
            System.out.println("Unesi ime osobe koja slusa: ");
            e.setImeOsobe(scan.nextLine());
            e.setPjesme(new ArrayList<>());
            for (; ; ) {
                p = new Pjesma();
                System.out.println("Unesi ime pjesme");
                p.setNaziv(scan.nextLine());
                System.out.println("Unesi izvodaca");
                p.setIzvodac(scan.nextLine());
                e.getPjesme().add(p);

                System.out.println("Još pjesama?");
                if (scan.nextLine().equals("ne")) {
                    break;
                }
            }
            evidencije.add(e);
            e.setImeOsobe(scan.nextLine());
            System.out.println("Još evidencija?");
            if (scan.nextLine().equals("ne")) {
                break;
            }
        }
        int sum = 0;
        count.addAll(e.getPjesme());
        for (Pjesma x: count
             ) {
                if(Collections.frequency(count,x)>1){
                    sum++;
                }
        }
        Map<Pjesma,Integer> mapa = new HashMap<>();
        evidencije.forEach((evidencija -> {
            evidencija.getPjesme().forEach((pjesma -> {
                mapa.put(pjesma,mapa.containsKey(pjesma)) ? mapa.get(pjesma)+1:1);
            });
        });
    }




}
