package deskar;

public abstract class Human extends Object {

    public abstract void knjizi();

    private String ime;
    private String prezime;
    private int visina;
    private boolean alive;

    public Human(String ime, String prezime, int visina, boolean alive) {
        this.ime = ime;
        this.prezime = prezime;
        this.visina = visina;
        this.alive = alive;
    }


    public Human() {

    }


    public String getIme() {
        if (ime.equals("prazno")) {
            return "prazno";
        }
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public int getVisina() {
        return visina;
    }

    public void setVisina(int visina) {
        this.visina = visina;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    protected void upit() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (visina != human.visina) return false;
        if (alive != human.alive) return false;
        if (ime != null ? !ime.equals(human.ime) : human.ime != null) return false;
        return prezime != null ? prezime.equals(human.prezime) : human.prezime == null;
    }

    @Override
    public int hashCode() {
        int result = ime != null ? ime.hashCode() : 0;
        result = 31 * result + (prezime != null ? prezime.hashCode() : 0);
        result = 31 * result + visina;
        result = 31 * result + (alive ? 1 : 0);
        return result;
    }

    public String pozdravi() {
        return "Dobar dan";
    }
}
