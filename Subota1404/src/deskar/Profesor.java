package deskar;

public class Profesor extends Human {

    private Fakultet fakultet;


    public Profesor(String ime, String prezime, int visina, boolean alive) {
        super(ime, prezime, visina, alive);
    }


    public Profesor() {

    }


    public Fakultet getFakultet() {

        return fakultet;
    }

    public void setFakultet(Fakultet fakultet) {
        this.fakultet = fakultet;
    }

    public void metoda1() {
        super.getIme();
    }

    @Override
    public String getIme() {
        return "Pero";
    }

    @Override
    public String pozdravi() {
        return "Dobar dan profesore";
    }

    @Override
    public void knjizi(){
        System.out.println("Na 220 + na 120 -");
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Profesor profesor = (Profesor) o;

        return fakultet != null ? fakultet.equals(profesor.fakultet) : profesor.fakultet == null;
    }

    @Override
    public int hashCode() {
        return fakultet != null ? fakultet.hashCode() : 0;
    }
}
