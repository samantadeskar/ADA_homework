package deskar.evidencija;

import javax.swing.*;

public class Start {
    Evidencija e;

    public Start() {
        do {
            e = new Evidencija(JOptionPane.showInputDialog("Unesi naziv evidencije"));
            e.setMjesto(new Mjesto(JOptionPane.showInputDialog("Unesi mjesto:"), 1));
            if (e.getNaziv().equals("") || e.getMjesto().getNaziv().equals("")) {
                JOptionPane.showMessageDialog(null, "Unesi naziv evidencije i/ili mjesta");
            }
        } while (e.getNaziv().equals("") || e.getMjesto().getNaziv().equals(""));
        ispisi();
    }

    public static void main(String[] args) {
        new Start();
    }

    public void ispisi() {
        System.out.println(e.getNaziv());
        System.out.println(e.getMjesto().getNaziv());
    }
}
