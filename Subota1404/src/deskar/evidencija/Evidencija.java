package deskar.evidencija;

public class Evidencija {

    private String naziv;
    Mjesto mjesto;

    public Mjesto getMjesto() {
        return mjesto;
    }

    public void setMjesto(Mjesto mjesto) {
        this.mjesto = mjesto;
    }

    public Evidencija(String naziv) {
        this.naziv = naziv;

    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

}
