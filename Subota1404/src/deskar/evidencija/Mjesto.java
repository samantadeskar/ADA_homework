package deskar.evidencija;

public class Mjesto {

    private String naziv;
    private int sifra;

    public Mjesto(String naziv, int sifra) {
        this.naziv = naziv;
        this.sifra = sifra;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getSifra() {
        return sifra;
    }

    public void setSifra(int sifra) {
        this.sifra = sifra;
    }
}
