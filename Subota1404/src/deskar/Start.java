package deskar;

import javax.swing.*;

//pojo - plain only-java object
public class Start {
    Profesor p;
    Student s;

    public Start() {

        Human[] ljudi = new Human[2];


        p = new Profesor(JOptionPane.showInputDialog("Unesi ime: "), "Peric", 180, true);
        p.setFakultet(new Fakultet("FFOS", "Jagerova"));

        s = new Student(JOptionPane.showInputDialog("Unesi ime;"), JOptionPane.showInputDialog("Unesi prezime"), 160, true);
        s.setFakultet(new Fakultet("FERIT", "Trpimirova"));

        ispisi();

        System.out.println(p.pozdravi());
        System.out.println(s.pozdravi());
        ;

        p.knjizi();
        s.knjizi();

        ljudi[0] = p;
        ljudi[1] = s;

        knjiziSve(ljudi);

        new UnosOsoba();
        new UnosFakulteta();

        Posao<Profesor> pp = new Posao<>();
        pp.odradi();

        Posao<Student> ps = new Posao<>();
        ps.odradi();
    }

    private void knjiziSve(Human[] n) {
        for (Human c : n
                ) {
            c.knjizi();
        }
    }

    private void ispisi() {
        System.out.println(p.getFakultet().getNaziv());
        System.out.println(s.getFakultet().getNaziv());
    }

    public static void main(String[] args) {

        new Start();


    }

}
