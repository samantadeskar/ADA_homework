package deskar;

public class Student extends Human{

    private Fakultet fakultet;

    @Override
    public void knjizi() {
        System.out.println("4662 +");
    }

    public Student(String ime, String prezime, int visina, boolean alive) {
        super(ime, prezime, visina, alive);
    }

    public Fakultet getFakultet() {
        return fakultet;
    }

    public void setFakultet(Fakultet fakultet) {
        this.fakultet = fakultet;
    }

    public Student() {
    }

    private Smjer smjer;

    public Smjer getSmjer() {
        return smjer;
    }

    public void setSmjer(Smjer smjer) {
        this.smjer = smjer;
    }
}
